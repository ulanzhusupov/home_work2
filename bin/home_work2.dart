import 'dart:io';

void main() {
  printCurrencies();
  print("1) Введите 1, если хотите обменять другую валюту на сом.\n2) Введите 2, если хотите обменять сом на другую валюту.");
  stdout.write('Введите: ');
  var a = stdin.readLineSync();

  if(a == "1") {
    currencyToSom();
  } else if(a == "2") {
    somToCurrencies();
  } else {
    print("Ошибка ввода. Попробуйте еще раз!");
  }
}


void printCurrencies(){
  String currencies = """
Курс на сегодня:
1. USD (покупка: 88.25, продажа: 88.44)
2. EUR (покупка: 93.90, продажа: 94.90)
3. RUB (покупка: 0.890, продажа: 0.919)
4. KZT (покупка: 0.1302, продажа: 0.2017)
""";
  print(currencies);
}

void currencyToSom() {
  String currencies = """
Введите валюту, которая у вас есть:
USD
EUR
RUB
KZT
""";
  print(currencies);
  stdout.write('Введите: ');

  String curr = stdin.readLineSync()!;

  if(curr == "USD") {
    print("Сколько $curr хотите обменять на сомы?");
    stdout.write('Введите: ');
    double amount = double.parse(stdin.readLineSync()!);
    String summ = (amount * 88.44).toStringAsFixed(2);

    print("Обмен $amount USD на сумму $summ сом.");
  } else if(curr == "EUR") {
    print("Сколько $curr хотите обменять на сомы?");
    stdout.write('Введите: ');
    double amount = double.parse(stdin.readLineSync()!);
    String summ = (amount * 94.90).toStringAsFixed(2);

    print("Обмен $amount EUR на сумму $summ сом.");
  } else if(curr == "RUB") {
    print("Сколько $curr хотите обменять на сомы?");
    stdout.write('Введите: ');
    double amount = double.parse(stdin.readLineSync()!);
    String summ = (amount * 0.919).toStringAsFixed(2);

    print("Обмен $amount RUB на сумму $summ сом.");
  } else if(curr == "KZT") {
    print("Сколько $curr хотите обменять на сомы?");
    stdout.write('Введите: ');
    double amount = double.parse(stdin.readLineSync()!);
    String summ = (amount * 0.2017).toStringAsFixed(2);

    print("Обмен $amount KZT на сумму $summ сом.");
  } else {
    print("Ошибка ввода. Попробуйте еще раз!");
  }
}

void somToCurrencies() {
  String currencies = """
Введите валюту, на которую хотите обменять сомы:
USD
EUR
RUB
KZT
""";
  print(currencies);
  stdout.write('Введите: ');

  String curr = stdin.readLineSync()!;

  if(curr == "USD") {
    print("Сколько сомов хотите обменять на $curr?");
    stdout.write('Введите: ');
    double amount = double.parse(stdin.readLineSync()!);
    String summ = (amount / 88.25).toStringAsFixed(2);

    print("Обмен $amount сомов на $summ $curr.");
  } else if(curr == "EUR") {
    print("Сколько сомов хотите обменять на $curr?");
    stdout.write('Введите: ');
    double amount = double.parse(stdin.readLineSync()!);
    String summ = (amount / 93.90).toStringAsFixed(2);

    print("Обмен $amount сомов на $summ $curr.");
  } else if(curr == "RUB") {
    print("Сколько сомов хотите обменять на $curr?");
    stdout.write('Введите: ');
    double amount = double.parse(stdin.readLineSync()!);
    String summ = (amount / 0.890).toStringAsFixed(2);

    print("Обмен $amount сомов на $summ $curr.");
  } else if(curr == "KZT") {
    print("Сколько сомов хотите обменять на $curr?");
    stdout.write('Введите: ');
    double amount = double.parse(stdin.readLineSync()!);
    String summ = (amount / 0.1302).toStringAsFixed(2);

    print("Обмен $amount сомов на $summ $curr.");
  } else {
    print("Ошибка ввода. Попробуйте еще раз!");
  }

}